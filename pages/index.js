import { MDBContainer, MDBRow, MDBCol } from "mdbreact";


export default function Home() {
  return (


        <MDBContainer className="home-container pt-5 d-flex  flex-column justify-content-center">
        <MDBRow>
          <MDBCol md="6" className="d-flex flex-column justify-content-center align-items-center">
             <h1 className="home-name headers">Nicolo Cabrera</h1>
             <p className="home-desrciption">Web Developer</p>
          </MDBCol> 
          <MDBCol md="6" className="d-flex justify-content-center align-items-center">
             <img src="/nicolo-website.jpg" alt="nicolo-website" className="img-fluid img-circle rounded-circle home-image"/>
          </MDBCol>
        </MDBRow>
        </MDBContainer>
       
    )

}
