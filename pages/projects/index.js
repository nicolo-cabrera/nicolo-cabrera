import {MDBContainer, MDBRow, MDBCol, MDBIcon} from 'mdbreact'
export default function Projects() {

	return(
			<MDBContainer className="projects-container my-5">
				<MDBRow className="pt-5">
					<MDBCol md="6" className="d-flex justify-content-center align-items-center">
						<div className="pb-5">
							<h3 className="projects-description mb-4">Coronavirus Tracker</h3>
							<div>
								<div>
									<MDBIcon icon="globe" /> <a href="https://covid-tracker-gamma.vercel.app/" target="_blank"> Live Website : Coronavirus Tracker</a>	
								</div>
								<div>
									<MDBIcon fab icon="gitlab" /> <a href="https://gitlab.com/nicolo-cabrera/covid-tracker" target="_blank">Gitlab Repository</a>
								</div>
							</div>
						</div>
					</MDBCol>
					<MDBCol md="6">
						<img src="/corona-virus-tracker.PNG" alt="corona virus tracker" className="img-fluid" />
					</MDBCol>
				</MDBRow>
				<MDBRow className="pt-5">
					<MDBCol md="6" className="d-flex justify-content-center align-items-center">
						<div className="pb-5">
							<h3 className="projects-description mb-4">Budget Tracker</h3>
							<div>
								<div>
									<MDBIcon icon="globe" /> <a href="https://capstone3-client.vercel.app/" target="_blank"> Live Website : Budget Tracker</a>	
								</div>
								<div>
									<MDBIcon fab icon="gitlab" /> <a href="https://gitlab.com/nicolo-cabrera/capstone3-client" target="_blank">Gitlab Repository</a>
								</div>
							</div>
						</div>
					</MDBCol>
					<MDBCol md="6">
						<img src="/budget.PNG" alt="budget" className="img-fluid" />
					</MDBCol>
				</MDBRow>
			</MDBContainer>
		)
}