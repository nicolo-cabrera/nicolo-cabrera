import {MDBContainer, MDBRow, MDBCol} from 'mdbreact'
import { MDBCard, MDBCardTitle, MDBBtn, MDBCardGroup, MDBCardImage, MDBCardText, MDBCardBody } from "mdbreact";
export default function technologies() {
	return(
			<MDBContainer className="technologies-container mt-5 d-flex align-items-center">
				<MDBRow className="row-cols-sm-2 row-cols-md-3 row-cols-lg-4 row-cols-xl-5">
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/react.png" alt="React" top hover
						    overlay="white-slight" />
						   <MDBCardTitle className="text-center technology-description">React</MDBCardTitle>


					</MDBCol> 
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/javascript.png" alt="Javascript" top hover
						    overlay="white-slight" />
						    <MDBCardTitle className="text-center technology-description">Javascript</MDBCardTitle>

					</MDBCol> 
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/node.png" alt="NodeJS" top hover
						    overlay="white-slight" />
						    <MDBCardTitle className="text-center technology-description">NodeJS</MDBCardTitle>

					</MDBCol> 
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/mongo.png" alt="MongoDB" top hover
						    overlay="white-slight" />
						    <MDBCardTitle className="text-center technology-description">MongoDB</MDBCardTitle>

					</MDBCol> 
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/express2.png" alt="ExpressJS" top hover
						    overlay="white-slight"/>
						    <MDBCardTitle className="text-center technology-description">ExpressJS</MDBCardTitle>
					</MDBCol>
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/next.png" alt="NextJS" top hover
						    overlay="white-slight"/>
						    <MDBCardTitle className="text-center technology-description">NextJS</MDBCardTitle>

					</MDBCol>
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/bootstrap.png" alt="Bootstrap" top hover
						    overlay="white-slight"/>
						    <MDBCardTitle className="text-center technology-description">Bootstrap</MDBCardTitle>

					</MDBCol>
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/html.png" alt="HTML" top hover
						    overlay="white-slight"/>
						    <MDBCardTitle className="text-center technology-description">HTML5</MDBCardTitle>

					</MDBCol>
					<MDBCol sm= "6" md="2" className="pt-4">

						  <MDBCardImage src="/css.png" alt="CSS3" top hover
						    overlay="white-slight"/>
						    <MDBCardTitle className="text-center technology-description">CSS3</MDBCardTitle>

					</MDBCol>
				</MDBRow>
			</MDBContainer>
		)
}