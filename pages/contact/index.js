import {useState, useEffect} from 'react';
import { MDBContainer, MDBRow, MDBCol, MDBBtn, MDBIcon, MDBInput } from 'mdbreact';
/*Spinner*/
import LoadingOverlay from 'react-loading-overlay'
import BounceLoader from 'react-spinners/BounceLoader'
import { ToastContainer, toast } from 'react-toastify'


export default function contact() {

	const [name, setName] = useState("")
	const [email, setEmail] = useState("")
	const [subject, setSubject] = useState("")
	const [message, setMessage] = useState("")
	const [isActive, setIsActive] = useState(true)
	const [sending, setSending] = useState(false)

	useEffect(() => {
		if(name !== "" && email !== "" && subject !== "" && message !== "") return setIsActive(false)
		return setIsActive(true)
		
	},[name, email, subject, message])

	const sendMessage = (e) => {
		e.preventDefault()

		setSending(true)
		fetch(`${process.env.NEXT_PUBLIC_API_URI}api/send`, {
			method: "POST",

			headers : {
				"Content-Type" : "application/json"
			},

			body : JSON.stringify({
				name : name,
				email : email,
				subject : subject,
				message : message
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data == true) {

				setSending(false)
				setName("")
				setEmail("")
				setSubject("")
				setMessage("")

				toast.success('Email has been sent succesfully!', {
				position: "bottom-right",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				});
			} else {
				setSending(false)
				toast.error('There was an error. Try again.', {
				position: "bottom-right",
				autoClose: 4000,
				hideProgressBar: false,
				closeOnClick: true,
				pauseOnHover: true,
				draggable: true,
				progress: undefined,
				});
			}
		})
	}

	return (
		<LoadingOverlay active={sending} spinner={<BounceLoader />} text='Sending...'>
			<MDBContainer className="contact-container d-flex  pt-5 mt-3 flex-column justify-content-center">
			  <MDBRow>
			    <MDBCol md="6">
			    	<h1 className="text-center">My Contact Information</h1>
			    	<div className="icon">
				    	<div className="icon-item">
					    	<div>
					    		<MDBIcon icon="at"/> Gmail : cabrera.jonnicolo@gmail.com
					    	</div>
					    	<div>
					    		<MDBIcon icon="mobile-alt" /> Phone : +63 966 338 9271
					    	</div>
					    	<div>
					    		<MDBIcon fab icon="gitlab" /> Gitlab : <a href="https://gitlab.com/nicolo-cabrera" target="_blank">https://gitlab.com/nicolo-cabrera</a>
					    	</div>
					    	<div>
					    		<MDBIcon fab icon="linkedin" /> Linkedin : <a href="https://www.linkedin.com/in/nicolo-cabrera-1972b1185/" target="_blank">https://www.linkedin.com/in/nicolo-cabrera-1972b1185/</a>
					    	</div>
				    	</div>
			    	</div> 
			    </MDBCol>
			    <MDBCol md="6">
			    	<form onSubmit={e => sendMessage(e)}>
			    	  <h1 className="text-center">Contact Me</h1>
			    	  <div className="grey-text">
			    	    <MDBInput label="Your full name" icon="user" group type="text" validate error="wrong"
			    	      success="right" value={name} onChange={e => setName(e.target.value)}/>
			    	    <MDBInput label="Your email" icon="envelope" group type="email" validate error="wrong"
			    	      success="right" value={email} onChange={e => setEmail(e.target.value)}/>
			    	    <MDBInput label="Subject" icon="tag" group type="text" validate error="wrong"
			    	     success="right"  value={subject} onChange={e => setSubject(e.target.value)}/>
			    	    <MDBInput type="textarea" rows="2" label="Your message" icon="pencil-alt" 
			    	    value={message} onChange={e => setMessage(e.target.value)}/>
			    	    <ToastContainer
			    	    position="bottom-right"
			    	    autoClose={4000}
			    	    hideProgressBar={false}
			    	    newestOnTop={false}
			    	    closeOnClick
			    	    rtl={false}
			    	    pauseOnFocusLoss
			    	    draggable
			    	    pauseOnHover
			    	    />
			    	  </div>
			    	  <div className="text-center">
			    	  <MDBBtn outline color="black" type="submit" disabled={isActive}>
			    	    Send
			    	    <MDBIcon far icon="paper-plane" className="ml-1"/>
			    	  </MDBBtn>
			    	  </div>
			    	</form>
			    </MDBCol>
			  </MDBRow>
			</MDBContainer>
		</LoadingOverlay>


		)
}