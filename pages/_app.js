import {useState} from 'react'
import Router from 'next/router'
import '@fortawesome/fontawesome-free/css/all.min.css'; 
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbreact/dist/css/mdb.css';
import '../styles/globals.css';
import Head from 'next/head';
import 'react-toastify/dist/ReactToastify.css';

/*Components*/
import NavBar from '../components/NavBar';
import Footer from '../components/Footer';
import Loader from '../components/Loader';

function MyApp({ Component, pageProps }) {

  const [isLoading, setIsLoading] = useState(false)

  Router.events.on('routeChangeStart', () => {
      setIsLoading(true)
  });

  Router.events.on('routeChangeComplete', () => {
      setIsLoading(false)
  });

  Router.events.on('routeChangeError', () => {
      setIsLoading(false)
  });


  return (
  	<>
  		<Head>
  			<title>Nicolo Cabrera</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="description" content="Website Portfolio" />
        <meta name="keywords" content="Fullstack Web Developer, Web Developer, Porfolio" />
  		</Head>
	  	<NavBar />
      {
        isLoading ? 
        <Loader />
        :
        <Component {...pageProps} />
      }
	  	<Footer />
  	</>
  	)
}

export default MyApp
