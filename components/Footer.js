import { MDBCol, MDBContainer, MDBRow, MDBFooter, MDBIcon } from "mdbreact";


export default function Footer() {

  return (

    <MDBFooter color="black" className="font-small pt-4">
      <MDBContainer fluid className="d-flex  flex-column justify-content-center">
        <MDBRow>
          <MDBCol md="6" className="d-flex flex-column justify-content-center align-items-center">
            <h5 className="title">Contact</h5>
            <div>
              <div>
                <MDBIcon icon="at" /> Gmail : cabrera.jonnicolo@gmail.com
              </div>
              <div>
                <MDBIcon icon="mobile-alt" /> Phone : +63 966 338 9271
              </div>
            </div> 
          </MDBCol>
          <MDBCol md="6" className="d-flex flex-column justify-content-center align-items-center">
            <h5 className="title">Links</h5>
              <div>
                <div>
                  <MDBIcon fab icon="gitlab" /> Gitlab : <a href="https://gitlab.com/nicolo-cabrera" target="_blank">https://gitlab.com/nicolo-cabrera</a>
                </div>
                <div>
                  <MDBIcon fab icon="linkedin" /> Linkedin : <a href="https://www.linkedin.com/in/nicolo-cabrera-1972b1185/" target="_blank">https://www.linkedin.com/</a>
                </div>
              </div> 
          </MDBCol>
        </MDBRow>
      </MDBContainer>
      <div className="footer-copyright text-center py-3">
        <MDBContainer fluid>
          &copy; {new Date().getFullYear()} Copyright: Jon Nicolo B. Cabrera
        </MDBContainer>
      </div>
    </MDBFooter>

    )
}