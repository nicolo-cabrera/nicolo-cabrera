import Head from 'next/head';
import Link from 'next/link';
import { useRouter } from "next/router";
import { useState } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBDropdownToggle} from "mdbreact";


export default function NavBar() {

  const [isOpen, setIsOpen] = useState(false)
  const router = useRouter();
  return (

       <MDBNavbar color="black" dark expand="md" scrolling fixed="top">
       <Link href="/">
           <a className="navbar-brand">Nicolo Cabrera</a>
       </Link>
         <MDBNavbarToggler onClick={() => setIsOpen(!isOpen)} />
         <MDBCollapse isOpen={isOpen} navbar>
           <MDBNavbarNav right>
           <Link href="/">
               <a className={router.pathname == "/" ? "nav-link text-white active" : "nav-link text-white"}>Home</a>
           </Link>
           <Link href="/projects">
               <a className={router.pathname == "/projects" ? "nav-link text-white active" : "nav-link text-white"}>Projects</a>
           </Link>
           <Link href="/technologies">
               <a className={router.pathname == "/technologies" ? "nav-link text-white active" : "nav-link text-white"}>Technologies</a>
           </Link>
           <Link href="/contact">
               <a className={router.pathname == "/contact" ? "nav-link text-white active" : "nav-link text-white"}>Contact</a>
           </Link>
           </MDBNavbarNav>
         </MDBCollapse>
       </MDBNavbar>
    )

}
